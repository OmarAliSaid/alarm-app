package com.example.android.alarm;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;

public class Setting extends AppCompatActivity implements OnCheckedChangeListener{

    SharedPreferences sharedpreferences ;
    SharedPreferences.Editor editor ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        RadioButton vibrate =(RadioButton)findViewById(R.id.radioBtn_tone);
        vibrate.setOnCheckedChangeListener(this);
        sharedpreferences = this.getSharedPreferences("Setting", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        if(vibrate.isChecked()){
            editor.putString("notifyType","vibrate");
            editor.commit();
        }else{
            editor.putString("notifyType","tone");
            editor.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b==true){
            editor.putString("notifyType","vibrate");
            editor.commit();
        }else{
            editor.putString("notifyType","tone");
            editor.commit();
        }
    }
}
