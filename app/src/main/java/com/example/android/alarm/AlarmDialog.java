package com.example.android.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

public class AlarmDialog extends Activity implements Button.OnClickListener{

    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new Dialog(this);

        dialog.setContentView(R.layout.dialog);
        dialog.setTitle("Reminder!! Open Your Store");

        final TextView notifyMsg=(TextView)dialog.findViewById(R.id.notifiy);
        Button Snooze=(Button)dialog.findViewById(R.id.btn_snooze);
        Snooze.setOnClickListener(this);
        Button Cancel=(Button)dialog.findViewById(R.id.btn_cancel);
        Cancel.setOnClickListener(this);
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_cancel:

                dialog.cancel();
                this.finish();
                break;

            case R.id.btn_snooze:

                Intent intent = new Intent(this, AlarmService.class);
                PendingIntent pendingIntent =
                        PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                long currentTimeMillis = System.currentTimeMillis();
                long nextUpdateTimeMillis = currentTimeMillis + 5 * DateUtils.SECOND_IN_MILLIS;
                Calendar calender = Calendar.getInstance();
                calender.setTimeInMillis(nextUpdateTimeMillis);

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, calender.getTimeInMillis(), pendingIntent);
                this.finish();
                break;
        }
    }
}
