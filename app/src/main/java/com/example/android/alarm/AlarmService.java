package com.example.android.alarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

/**
 * Created by OmarAli on 07/09/2015.
 */
public class AlarmService extends Service {
    public NotificationManager mManager;

    @Override
    public IBinder onBind(Intent arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate()
    {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        SharedPreferences pref = this.getSharedPreferences("Setting", 0);
        String notifyType=pref.getString("notifyType", null);

        mManager = (NotificationManager) this.getApplicationContext().getSystemService(this.getApplicationContext().NOTIFICATION_SERVICE);

        // Replace NextActivity with the activity you want to go to when notification clicked
        Intent intent1 = new Intent(this.getApplicationContext(),NextActivity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity( this.getApplicationContext(),0, intent1,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this);
        builder.setContentIntent(pendingNotificationIntent)
                .setSmallIcon(R.drawable.ic_launcher).setTicker("This is a test message!").setWhen(System.currentTimeMillis())
                .setAutoCancel(true).setContentTitle("AlarmManagerDemo")
                .setContentText("hello");

        if(notifyType!=null && notifyType.equals("vibrate")){
            builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 } );
            Log.d("type : ",notifyType);
        }else{
            Uri path=Uri.parse("android.resource://com.example.android.alarm/" + R.raw.sound);
            builder.setSound(path);
        }

        mManager.notify(0, builder.build());

        Intent i = new Intent();
        i.setClass(this, AlarmDialog.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
}
