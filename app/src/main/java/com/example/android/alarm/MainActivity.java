package com.example.android.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity  {

    private PendingIntent pendingIntent;
    EditText time;
    SharedPreferences sharedpreferences ;
    SharedPreferences.Editor editor ;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn_alarm = (Button)findViewById(R.id.btn_alram);
        time=(EditText)findViewById(R.id.time);
        btn_alarm.setOnClickListener(HomeButtons);
        sharedpreferences = this.getSharedPreferences("AlarmPref", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(MainActivity.this,Setting.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private View.OnClickListener HomeButtons = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId()==R.id.btn_alram){

                String Time=time.getText().toString();

                editor.putString("AlarmTime",Time);
                editor.commit();

                AlarmSetter Alarm = new AlarmSetter();
                Alarm.setTime(Time);
                if(Alarm.getAlarm().getTimeInMillis() < Calendar.getInstance().getTimeInMillis()){
                    Alarm.updateOneDay();
                }
                Intent myIntent = new Intent(MainActivity.this, AlarmReceiver.class);
                pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.RTC, Alarm.getAlarm().getTimeInMillis(), 1000*60*60*24, pendingIntent);
            }
        }
    };
}
