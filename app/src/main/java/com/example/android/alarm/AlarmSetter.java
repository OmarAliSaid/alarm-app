package com.example.android.alarm;

import java.util.Calendar;

/**
 * Created by OmarAli on 08/09/2015.
 */
public class AlarmSetter {

    int minute,hour,AM_PM_Identifier;
    String AM_PM;
    Calendar calendar;

    public AlarmSetter(){
        calendar = Calendar.getInstance();
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getAM_PM() {
        return AM_PM;
    }

    public void setAM_PM(String AM_PM) {
        this.AM_PM = AM_PM;
    }

    public void setTime(String time){

        setHour(Integer.valueOf(time.substring(0, 2)));
        setMinute(Integer.valueOf(time.substring(3, 5)));
        setAM_PM(time.substring(6, 8));

        if(getAM_PM().equals("PM")){
            AM_PM_Identifier=1;
        }else { AM_PM_Identifier=0; }

        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) );

        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) );

        calendar.set(Calendar.HOUR_OF_DAY, getHour());
        calendar.set(Calendar.MINUTE, getMinute());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.AM_PM, AM_PM_Identifier );

    }

    public void updateOneDay(){
        calendar.add(Calendar.DAY_OF_MONTH,1);
    }

    public Calendar getAlarm(){ return calendar; }
}
