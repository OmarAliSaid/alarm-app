package com.example.android.alarm;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Created by OmarAli on 07/09/2015.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public NotificationManager mManager;
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if( "android.intent.action.BOOT_COMPLETED".equals (intent.getAction()) ){

            SharedPreferences pref = context.getSharedPreferences("AlarmPref", 0);
            String time = pref.getString("AlarmTime",null);
            if(time!=null){

                AlarmSetter Alarm = new AlarmSetter();
                Alarm.setTime(time);
                if(Alarm.getAlarm().getTimeInMillis() < System.currentTimeMillis()){
                    Alarm.updateOneDay();
                }
                Intent srvIntent = new Intent(context, AlarmService.class);
                PendingIntent pendingIntent = PendingIntent.getService(context, 0, srvIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                // Use context argument to access service
                AlarmManager alarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

                // Repeat every day
                alarm.setRepeating(AlarmManager.RTC, Alarm.getAlarm().getTimeInMillis(), 1000*60*60*24, pendingIntent);
            }

        }else{
            Intent service1 = new Intent(context, AlarmService.class);
            context.startService(service1);
        }

    }

}

